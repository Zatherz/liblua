require "./types"
require "./enums"

lib LibLua
  fun gethook = lua_gethook(State) : Hook
  fun gethookcount = lua_gethookcount(State) : Int
  fun gethookmask = lua_gethookmask(State) : Int
  fun getinfo = lua_getinfo(State, what : CString, ar : Debug*) : Int
  fun getlocal = lua_getlocal(State, ar : Debug*, n : Int) : CString
  fun getstack = lua_getstack(State, level : Int, ar : Debug*) : Int
  fun getupvalue = lua_getupvalue(State, funcindex : Int, n : Int) : CString
  fun sethook = lua_sethook(State, f : Hook, mask : HookMask, count : Int) : Void
  fun setlocal = lua_setlocal(State, ar : Debug*, n : Int) : CString
  fun setupvalue = lua_setupvalue(State, funcindex : Int, n : Int) : CString
  fun upvalueid = lua_upvalueid(State, funcindex : Int, n : Int) : Void*
  fun upvaluejoin = lua_upvaluejoin(State, funcindex1 : Int, n1 : Int, funcindex2 : Int, n2 : Int) : Void
end
