require "./types"
require "./enums"

lib LibLua
  fun l_addchar = luaL_addchar(Buffer, c : Char) : Void
  fun l_addlstring = luaL_addlstring(Buffer, s : CString, l : SizeT) : Void
  fun l_addsize = luaL_addsize(Buffer, n : SizeT) : Void
  fun l_addstring = luaL_addstring(Buffer, s : CString) : Void
  fun l_addvalue = luaL_addvalue(Buffer) : Void
  fun l_argcheck = luaL_argcheck(State, cond : Int, arg : Int, extramsg : CString) : Void
  fun l_argerror = luaL_argerror(State, arg : Int, extramsg : CString) : Int
  fun l_buffinit = luaL_buffinit(State, Buffer) : Void
  fun l_buffinitsize = luaL_buffinitsize(State, Buffer, sz : SizeT) : Char*
  fun l_callmeta = luaL_callmeta(State, obj : Int, e : CString) : Int
  fun l_checkany = luaL_checkany(State, arg : Int) : Void
  fun l_checkinteger = luaL_checkinteger(State, arg : Int) : Integer
  fun l_checklstring = luaL_checklstring(State, arg : Int, l : SizeT*) : CString
  fun l_checknumber = luaL_checknumber(State, arg : Int) : Number
  fun l_checkoption = luaL_checkoption(State, arg : Int, def : CString, lst : CString*) : Int
  fun l_checkstack = luaL_checkstack(State, sz : Int, msg : CString) : Void
  fun l_checkstring = luaL_checkstring(State, arg : Int) : CString
  fun l_checktype = luaL_checktype(State, arg : Int, t : Int) : Void
  fun l_checkudata = luaL_checkudata(State, arg : Int, tname : CString) : Void*
  fun l_checkversion = luaL_checkversion(State) : Void
  # fun l_dofile MACRO
  # fun l_dostring MACRO
  fun l_error = luaL_error(State, fmt : CString, ...) : Int
  fun l_execresult = luaL_execresult(State, stat : Int) : Int
  fun l_fileresult = luaL_fileresult(State, stat : Int, fname : CString) : Int
  fun l_getmetafield = luaL_getmetafield(State, obj : Int, e : CString) : Int
  fun l_getmetatable = luaL_getmetatable(State, tname : CString) : Int
  fun l_getsubtable = luaL_getsubtable(State, idx : Int, fname : CString) : Int
  fun l_gsub = luaL_gsub(State, s : CString, p : CString, r : CString) : CString
  fun l_len = luaL_len(State, index : Int) : Integer
  fun l_loadbuffer = luaL_loadbuffer(State, buff : CString, sz : SizeT, name : CString) : Int
  fun l_loadbufferx = luaL_loadbufferx(State, buff : CString, sz : SizeT, name : CString, mode : CString) : Int
  fun l_loadfile = luaL_loadfile(State, filename : CString) : Int
  fun l_loadfilex = luaL_loadfilex(State, filename : CString, mode : CString) : Int
  fun l_loadstring = luaL_loadstring(State, s : CString) : Int
  # fun l_newlib MACRO
  # fun l_newlibtable MACRO
  fun l_newmetatable = luaL_newmetatable(State, tname : CString) : Int
  fun l_newstate = luaL_newstate : State
  fun l_openlibs = luaL_openlibs(State) : Void
  # fun l_opt MACRO
  fun l_optinteger = luaL_optinteger(State, arg : Int, d : Integer) : Integer
  fun l_optlstring = luaL_optlstring(State, arg : Int, d : CString, l : SizeT) : CString
  fun l_optnumber = luaL_optnumber(State, arg : Int, d : Number) : Number
  fun l_optstring = luaL_optstring(State, arg : Int, d : CString) : CString
  fun l_prepbuffer = luaL_prepbuffer(Buffer) : CString
  fun l_prepbuffsize = luaL_prepbuffsize(Buffer, sz : SizeT) : CString
  fun l_pushresult = luaL_pushresult(Buffer) : Void
  fun l_pushresultsize = luaL_pushresultsize(Buffer, sz : SizeT) : Void
  fun l_ref = luaL_ref(State, t : Int) : Int
  fun l_requiref = luaL_requiref(State, modname : CString, openf : CFunction, glb : Int) : Void
  fun l_setfuncs = luaL_setfuncs(State, l : Reg*, nup : Int) : Void
  fun l_setmetatable = luaL_setmetatable(State, tname : CString) : Void
  fun l_testudata = luaL_testudata(State, arg : Int, tname : CString) : Void*
  fun l_tolstring = luaL_tolstring(State, idx : Int, len : SizeT*) : CString
  fun l_traceback = luaL_traceback(State, l1 : State, msg : CString, level : Int) : Void
  fun l_typename = luaL_typename(State, index : Int) : CString
  fun l_unref = luaL_unref(State, t : Int, ref : Int) : Void
  fun l_where = luaL_where(State, lvl : Int) : Void
end
