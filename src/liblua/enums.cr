lib LibLua
  {% if flag?(:x86_64) %}
    LUAI_MAXSTACK = 1000000
  {% else %}
    LUAI_MAXSTACK = 15000
  {% end %}

  enum MathOperator
    Add
    Subtract
    Multiply
    Modulo
    Power
    Divide
    FloorDivide
    BitwiseAnd
    BitwiseOr
    BitwiseXor
    ShiftLeft
    ShiftRight
    Negation
    BinaryNot
  end

  enum ComparisonOperator
    Equality
    LessThan
    LessThanOrEqual
  end

  enum GCOperation
    Stop
    Restart
    Collect
    Count
    CountB
    Step
    SetPause
    SetStepMultiplier
    IsRunning
  end

  enum LoadError
    Ok
    ErrSyntax         = 3
    OutOfMemory
    GCMetaMethodError
  end

  enum PcallError
    Ok
    RuntimeError        = 2
    OutOfMemory         = 4
    MessageHandlerError = 6
    GCMetaMethodError   = 5
  end

  enum ThreadStatus
    Ok
    Yield
  end

  enum Type
    None          = -1
    Nil
    Boolean
    LightUserData
    Number
    String
    Table
    Function
    Userdata
    Thread
  end

  @[Flags]
  enum HookMask
    Call   = 1 << 0
    Return = 1 << 1
    Line   = 1 << 2
    Count  = 1 << 3
  end

  @[Flags]
  enum CallOptions
    MultiReturn = -1
  end

  enum PseudoIndex
    Registry = -LUAI_MAXSTACK - 1000
  end
end
