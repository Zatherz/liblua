lib LibLua
  IDSIZE = 60

  # C aliases
  alias Char = LibC::Char
  alias Long = LibC::Long
  alias CString = Char*
  alias SizeT = LibC::SizeT
  alias Int = LibC::Int
  alias ULong = LibC::ULong
  alias UInt = LibC::UInt
  alias Double = LibC::Double
  {% if flag?(:x86_64) %}
    alias PtrDiffT = LibC::Int64T
  {% else %}
    alias PtrDiffT = LibC::Int32T
  {% end %}
  alias UChar = UInt8
  alias File = Void # for File*

  # Lua types
  # hide the implementation detail
  alias State = Void*
  alias Buffer = Void*

  alias Alloc = Void*, Void*, SizeT -> Void*
  alias CFunction = State -> Int
  {% if flag?(:lua_integer_long) %}
      alias Unsigned = ULong
      alias Integer = Long
    {% elsif flag?(:lua_integer_int) %}
      alias Unsigned = UInt
      alias Integer = Int
    {% else %}
        alias Integer = LibC::LongLong
    {% end %}
  alias KContext = PtrDiffT
  alias KFunction = State, Int, KContext -> Int
  alias Number = Double
  alias Reader = State, Void*, SizeT* -> CString
  alias Writer = State, Void*, SizeT, Void* -> Int
  alias Hook = State, Debug* -> Void

  struct Reg
    name : CString
    func : CFunction
  end

  struct Stream
    f : File*
    closef : CFunction
  end

  struct Debug
    event : Int
    name : CString
    namewhat : CString
    what : CString
    source : CString
    currentline : Int
    linedefined : Int
    lastlinedefined : Int
    nups : Int
    nparams : Int
    isvararg : Char
    istailcall : Char
    short_src : Char[IDSIZE]
  end
end
