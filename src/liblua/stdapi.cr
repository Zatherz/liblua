require "./types"
require "./enums"

lib LibLua
  fun absindex = lua_absindex(State, idx : Int) : Int
  fun arith = lua_arith(State, op : MathOperator) : Void
  fun atpanic = lua_atpanic(State, panicf : CFunction) : CFunction
  # fun call MACRO
  fun callk = lua_callk(State, nargs : Int, nresults : Int, ctx : KContext, k : KFunction) : Void
  fun checkstack = lua_checkstack(State, n : Int) : Int
  fun close = lua_close(State) : Void
  fun compare = lua_compare(State, index1 : Int, index2 : Int, op : ComparisonOperator) : Int
  fun concat = lua_concat(State, n : Int) : Void
  fun copy = lua_copy(State, fromidx : Int, toidx : Int) : Void
  fun createtable = lua_createtable(State, narr : Int, nrec : Int) : Void
  fun dump = lua_dump(State, writer : Writer, data : Void*, strip : Int) : Int
  fun error = lua_error(State) : Int
  fun gc = lua_gc(State, what : Int, data : Int) : Int
  fun getallocf = lua_getallocf(State, ud : Void**) : Alloc
  fun getfield = lua_getfield(State, index : Int, k : CString) : Int
  fun getextraspace = lua_getextraspace(State) : Void*
  fun getglobal = lua_getglobal(State, name : CString) : Int
  fun geti = lua_geti(State, index : Int, i : Integer) : Int
  fun getmetatable = lua_getmetatable(State, index : Int) : Int
  fun gettable = lua_gettable(State, index : Int) : Int
  fun gettop = lua_gettop(State) : Int
  fun getuservalue = lua_getuservalue(State, index : Int) : Int
  fun insert = lua_insert(State, index : Int) : Void
  fun isboolean = lua_isboolean(State, index : Int) : Int
  fun iscfunction = lua_iscfunction(State, index : Int) : Int
  fun isfunction = lua_isfunction(State, index : Int) : Int
  fun isinteger = lua_isinteger(State, index : Int) : Int
  fun islightuserdata = lua_islightuserdata(State, index : Int) : Int
  fun isnil = lua_isnil(State, index : Int) : Int
  fun isnone = lua_isnone(State, index : Int) : Int
  fun isnoneornil = lua_isnoneornil(State, index : Int) : Int
  fun isnumber = lua_isnumber(State, index : Int) : Int
  fun isstring = lua_isstring(State, index : Int) : Int
  fun istable = lua_istable(State, index : Int) : Int
  fun isthread = lua_isthread(State, index : Int) : Int
  fun isuserdata = lua_isuserdata(State, index : Int) : Int
  fun isyieldable = lua_isyieldable(State, index : Int) : Int
  fun len = lua_len(State, index : Int) : Void
  fun load = lua_load(State, Reader, data : Void*, chunkname : CString, mode : CString) : Int
  fun newstate = lua_newstate(Alloc, ud : Void*) : State
  # fun newtable MACRO
  fun newthread = lua_newthread(State) : State
  fun newuserdata = lua_newuserdata(State, size : SizeT) : Void*
  fun next = lua_next(State, index : Int) : Int
  # fun numbertointeger MACRO
  # fun pcall MACRO
  fun pcallk = lua_pcallk(State, nargs : Int, nresults : Int, msgh : Int, ctx : KContext, k : KFunction) : Int
  fun pop = lua_pop(State, n : Int) : Void
  fun pushboolean = lua_pushboolean(State, b : Int) : Void
  fun pushcclosure = lua_pushcclosure(State, fn : CFunction, n : Int) : Void
  fun pushcfunction = lua_pushcfunction(State, fn : CFunction) : Void
  fun pushfstring = lua_pushfstring(State, fmt : CString, ...) : CString
  fun pushglobaltable = lua_pushglobaltable(State) : Void
  fun pushinteger = lua_pushinteger(State, n : Integer) : Void
  fun pushlightuserdata = lua_pushlightuserdata(State, p : Void*) : Void
  # fun pushliteral LITERAL C STRINGS ONLY
  fun pushlstring = lua_pushlstring(State, s : CString, len : SizeT) : CString
  fun pushnil = lua_pushnil(State) : Void
  fun pushnumber = lua_pushnumber(State, n : Number) : Void
  fun pushstring = lua_pushstring(State, s : CString) : CString
  fun pushthread = lua_pushthread(State) : Int
  fun pushvalue = lua_pushvalue(State, index : Int) : Void
  # fun pushvfstring VA_LIST
  fun rawequal = lua_rawequal(State, index1 : Int, index2 : Int) : Int
  fun rawget = lua_rawget(State, index : Int) : Int
  fun rawgeti = lua_rawgeti(State, index : Int, n : Integer) : Int
  fun rawgetp = lua_rawgetp(State, index : Int, p : Void*) : Int
  fun rawlen = lua_rawlen(State, index : Int) : SizeT
  fun rawset = lua_rawset(State, index : Int) : Void
  fun rawseti = lua_rawseti(State, index : Int, i : Integer) : Void
  fun rawsetp = lua_rawsetp(State, index : Int, p : Void*) : Void
  # fun register MACRO
  fun remove = lua_remove(State, index : Int) : Void
  fun replace = lua_replace(State, index : Int) : Void
  fun resume = lua_resume(State, from : State, nargs : Int) : Int
  fun rotate = lua_rotate(State, idx : Int, n : Int) : Void
  fun setallocf = lua_setallocf(State, f : Alloc, ud : Void*) : Void
  fun setfield = lua_setfield(State, index : Int, k : CString) : Void
  fun setglobal = lua_setglobal(State, name : CString) : Void
  fun seti = lua_seti(State, index : Int, n : Integer) : Void
  fun setmetatable = lua_setmetatable(State, index : Int) : Void
  fun settable = lua_settable(State, index : Int) : Void
  fun settop = lua_settop(State, index : Int) : Void
  fun setuservalue = lua_setuservalue(State, index : Int) : Void
  fun status = lua_status(State) : Int
  fun stringtonumber = lua_stringtonumber(State, s : CString) : SizeT
  fun toboolean = lua_toboolean(State, index : Int) : Int
  fun tocfunction = lua_tocfunction(State, index : Int) : CFunction
  fun tointeger = lua_tointeger(State, index : Int) : Integer
  fun tointegerx = lua_tointegerx(State, index : Int, isnum : Int*) : Integer
  fun tolstring = lua_tolstring(State, index : Int, len : SizeT*) : CString
  fun tonumber = lua_tonumber(State, index : Int) : Number
  fun tonumberx = lua_tonumberx(State, index : Int, isnum : Int*) : Number
  fun topointer = lua_topointer(State, index : Int) : Void*
  fun tostring = lua_tostring(State, index : Int) : CString
  fun tothread = lua_tothread(State, index : Int) : State
  fun touserdata = lua_touserdata(State, index : Int) : Void*
  fun type = lua_type(State, index : Int) : Int
  fun typename = lua_typename(State, Type) : CString
  fun upvalueindex = lua_upvalueindex(i : Int) : Int
  fun version = lua_version(State) : Number
  fun xmove = lua_xmove(from : State, to : State, n : Int) : Void
  fun yield = lua_yield(State, nresults : Int) : Int
  fun yieldk = lua_yieldk(State, nresults : Int, ctx : KContext, k : KFunction) : Int
end
