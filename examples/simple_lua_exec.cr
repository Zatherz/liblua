require "../src/liblua"

l = LibLua.l_newstate
LibLua.l_openlibs l
puts LibLua.gettop l
LibLua.l_loadstring(l, %q{
  return 1, 2, 3
})
LibLua.callk(l, 0, LibLua::CallOptions::MultiReturn, 0, nil)
puts LibLua.gettop l
LibLua.close l
