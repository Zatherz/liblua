require "../src/liblua"
require "http/client"

l = LibLua.l_newstate
LibLua.l_openlibs l
proc = ->(l : LibLua::State) do
  puts "Hello from Crystal!"
  puts HTTP::Client.get("http://whatthecommit.com/index.txt").body

  1
end
LibLua.pushcclosure(l, proc, 0)
LibLua.setglobal(l, "hello")
LibLua.l_loadstring(l, %q{
  hello()
})
LibLua.callk(l, 0, LibLua::CallOptions::MultiReturn, 0, nil)
LibLua.close l
