# LuaLib

WIP "Raw" bindings to the Lua API for Crystal.
All functions can be found under `LibLua`. `lua_` functions have no prefix. `luaL_` functions have a `l_` prefix.
